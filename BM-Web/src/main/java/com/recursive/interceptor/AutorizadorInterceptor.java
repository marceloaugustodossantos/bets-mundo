/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recursive.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 *
 * @author Marcelo Augusto
 */
public class AutorizadorInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String uri = request.getRequestURI();
        if (uri.endsWith("index")
                || uri.endsWith("login")
                || uri.endsWith("autenticar")
                || uri.contains("apostador")) {
            return true;
        }
        if (request.getSession().getAttribute("admin") != null) {
            return true;
        }
        response.sendRedirect("apostador/index");
        return false;
    }

}
