<%-- 
    Document   : jogo
    Created on : 21/05/2016, 11:41:09
    Author     : Marcelo Augusto
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Apostador</title>
        <link rel="stylesheet" href="/assets/css/bootstrap.min.css" type="text/css">
        <link  href="/css/style.css" rel="stylesheet" type="text/css">
        <script src="/assets/js/jquery.js" type="text/javascript"></script>
        <script src="/assets/js/bootstrap.js" type="text/javascript"></script>    
        <meta name="viewport" content="width=device-width, minimum=scale= 1.0, initial=scale=1, user-scalable=yes"/>   
    </head>
    <body id="indexBody">
        <header>
            <nav class="navbar navbar-inverse">
                <div class="container-fluid" >
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="navbar-header" >
                        <a class="navbar-brand" href="/admin/">
                            <!--    <img alt="Brand" height="25" width="30" src="/imagens/logo.png"> -->
                            <b >Bets Mundo</b>
                        </a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav"> 
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cadastrar<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="cadastrooperador">Apostador</a></li>
                                    <li><a href="cadastrojogo">Campeonatos, times e jogos</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Visualizar<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="operador">Operadores</a></li>
                                    <li><a href="#">Jogos</a></li>
                                    <li><a href="#">Apostas realizadas</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="settings">Configurações</a></li>
                            <li><a href="logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header> 

        <div class="container">

            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="text-center"><span class="glyphicon glyphicon-edit"></span> Editar partida</h2>
                    </div>
                    <div class="panel-body">
                        <h3>Detalhes da partida</h3>
                        <h5><b>Campeonato</b> </h5>
                        <h5><b>Mandante</b> ${jogo.mandante.nome}</h5>
                        <h5><b>Visitante</b> ${jogo.visitante.nome}</h5>
                        <h5><b>Data - hora</b> ${jogo.data} 
                            <a role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                Alterar
                            </a>
                        </h5>
                        <div class="collapse" id="collapseExample">
                            <div class="well">
                                <form action="alterarData">
                                    <input type="hidden" name="idJogo" value="${jogo.id}">
                                    <label for="cotacao">Alterar data / horário</label>
                                    <input style="width: 220px; margin-bottom: 10px" type="datetime-local" name="data" class="form-control" id="data" placeholder="Data e horário da partida">
                                    <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-save"></span> Salvar</button>   
                                </form>
                            </div>
                        </div>
                        <fieldset>
                            <legend>Cotações para esta partida</legend>
                            <form action="alterarCotacao">
                                <input type="hidden" name="idJogo" value="${jogo.id}">
                                <c:forEach var="cotacao" items="${jogo.cotacoes}">
                                    <div class="col-md-3" style="margin-bottom: 10px">                                
                                        <label for="cotacao">${cotacao.nome}</label>
                                        <input type="text" value="${cotacao.valor}" name="${cotacao.id}" required="on" class="form-control" id="cotacao">
                                    </div>
                                </c:forEach>
                                <a href="excluirPartida?idJogo=${jogo.id}" class="btn btn-danger" style="margin-left: 22%"><span class="glyphicon glyphicon-remove"></span> Excluir partida</a>
                                <button type="submit" class="btn btn-success" style="margin-left: 22%"><span class="glyphicon glyphicon-save"></span> Salvar alterações</button>
                            </form>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
